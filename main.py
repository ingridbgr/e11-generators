# Não altere a linha seguinte
import time

# Altere a linha seguinte adicionando zeros conforme o video do enunciado
MAX_RANGE = 5

def exercise_1(MAX_RANGE):
    number = 0
    lista = []
    final_list = [number for number in range(MAX_RANGE)]
    return final_list    



def exercise_1_yield(MAX_RANGE):
    index = 0
    while index < MAX_RANGE:
        yield index
        index = index + 1

#Nao altere a partir dessa linha para baixo !!

#Sem YIELD
start = time.process_time()

exercise_1(MAX_RANGE)

end = time.process_time() - start
print(f"-- Tempo de execução SEM YIELD: {end:.6f} segundos --")


# Com YIELD
start = time.process_time()

exercise_1_yield(MAX_RANGE)

end = time.process_time() - start
print(f"-- Tempo de execução COM YIELD: {end:.6f} segundos --")